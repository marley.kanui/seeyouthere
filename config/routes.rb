Rails.application.routes.draw do

  devise_for :users
  root 'events#index'
  get 'event_page', to: 'events#event_page'

  resources :events
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
